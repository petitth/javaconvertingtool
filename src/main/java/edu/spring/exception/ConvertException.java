package edu.spring.exception;

public class ConvertException extends Exception {

    public ConvertException(String message) {
        super(message);
    }
}
