package edu.spring.repository;

import org.junit.jupiter.api.Test;
import org.powermock.reflect.Whitebox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@SpringBootTest(classes = {AppProperties.class})
@ActiveProfiles("test")
public class AppPropertiesTest {

    private static final Logger LOG = LoggerFactory.getLogger(AppPropertiesTest.class);

    @Autowired
    private AppProperties appProperties;

    @Test
    public void getJsonProperties() {
        assertNotNull(appProperties.getJsonIdentFactor());
        LOG.debug("json :: ident factor = {}", appProperties.getJsonIdentFactor());
    }

    @Test
    public void getXmlProperties() {
        assertNotNull(appProperties.getXmlIdentFactor());
        assertNotNull(appProperties.getXmlDefaultHeader());
        assertNotNull(appProperties.getXmlDefaultRootStart());
        assertNotNull(appProperties.getXmlDefaultRootEnd());
        LOG.debug("xml :: ident factor = {}", appProperties.getXmlIdentFactor());
        LOG.debug("xml :: default header = {}", appProperties.getXmlDefaultHeader());
        LOG.debug("xml :: default root start = {}", appProperties.getXmlDefaultRootStart());
        LOG.debug("xml :: default root end = {}", appProperties.getXmlDefaultRootEnd());
    }

    @Test
    public void getScreenProperties() {
        assertNotNull(appProperties.getScreenHeight());
        assertNotNull(appProperties.getScreenWidth());
        LOG.debug("screen :: height = {}", appProperties.getScreenHeight());
        LOG.debug("screen :: width = {}", appProperties.getScreenWidth());
    }

    @Test
    public void getColors() {
        assertNotNull(appProperties.getColorDefaultResult());
        assertEquals(new Color(206,206,206), appProperties.getColorDefaultResult());
        assertNotNull(appProperties.getColorInput());
        assertEquals(new Color(254,254,226), appProperties.getColorInput());
        assertNotNull(appProperties.getColorError());
        assertEquals(new Color(231,168,84), appProperties.getColorError());
        assertNotNull(appProperties.getColorSuccess());
        assertEquals(new Color(130,196,108), appProperties.getColorSuccess());
        LOG.debug("color :: success = {}", appProperties.getColorSuccess());
        LOG.debug("color :: error = {}", appProperties.getColorError());
        LOG.debug("color :: input = {}", appProperties.getColorInput());
        LOG.debug("color :: default result = {}", appProperties.getColorDefaultResult());
    }

    @Test
    public void colorsErrors() throws Exception {
        assertEquals(Color.white, Whitebox.invokeMethod(appProperties, "getColor", ""));
        assertEquals(Color.white, Whitebox.invokeMethod(appProperties, "getColor", "123456"));
        assertEquals(Color.white, Whitebox.invokeMethod(appProperties, "getColor", "123:456"));
    }
}