# Releases

## 1.0.8

- add conversion from csv to json

## 1.0.7
- subfolder 'sources' removed, it's content moved to root
- log folder : temp folder is now dynamic 
- pom : dependencies upgraded

## 1.0.6
- fix log

## 1.0.5
- upgrade libraries

## 1.0.4
- improve the display of text cursor in left panel

## 1.0.3
- add status bar container text cursor position

## 1.0.2
- add conversion to lower cases
- add conversion to upper cases

## 1.0.1
- add display of application version in window header

## 1.0.0
- trimming of json, xml and generic files
- display with indentation of json and xml files
- conversion from json to xml
- conversion from xml to json